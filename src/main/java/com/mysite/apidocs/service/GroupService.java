package com.mysite.apidocs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysite.apidocs.mapper.GroupMapper;
import com.mysite.apidocs.model.Group;

@Service
public class GroupService {
    @Autowired
    private GroupMapper groupMapper;

    public List<Group> getGroups() {
        return groupMapper.getGroups();
    }
}