package com.mysite.apidocs.mapper;

import com.mysite.apidocs.model.Request;

public interface RequestMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Request record);

    int insertSelective(Request record);

    Request selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Request record);

    int updateByPrimaryKey(Request record);
}