package com.mysite.apidocs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysite.apidocs.model.Group;
import com.mysite.apidocs.service.GroupService;

@Controller
@RequestMapping("/group")
public class GroupController {
    @Autowired
    private GroupService groupService;

    @RequestMapping("")
    @ResponseBody
    public List<Group> getGroups() {
        return groupService.getGroups();
    }
}