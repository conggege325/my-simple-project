package com.mysite.apidocs.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("")
public class IndexController {
    @RequestMapping("/index")
    public ModelAndView index() {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("name", "admin");
        return new ModelAndView("index", model);
    }

    @RequestMapping("/welcome")
    public String welcome() {
        return "welcome";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> login(String username, String password, HttpServletRequest request) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        if ("admin".equals(username) && "`".equals(password)) {
            resultMap.put("success", true);
            request.getSession().setAttribute("islogin", true);
        } else {
            resultMap.put("success", false);
        }
        return resultMap;
    }
}
