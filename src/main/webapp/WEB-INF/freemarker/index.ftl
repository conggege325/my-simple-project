<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Test</title>
    <script src="${request.contextPath}/lib/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $.getJSON('${request.contextPath}/apidocs/group', function (result) {
            $.each(result, function(){
                $('<li />').html(this.name).appendTo('#groups');
            });
        });
    </script>
</head>
<body>
<h1>Hello, ${name}</h1>
<ul id="groups"></ul>
</body>
</html>
