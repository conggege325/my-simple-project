/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50619
Source Host           : 127.0.0.1:3306
Source Database       : apidocs

Target Server Type    : MYSQL
Target Server Version : 50619
File Encoding         : 65001

Date: 2016-12-28 16:52:05
*/

CREATE DATABASE IF NOT EXISTS apidocs
  DEFAULT CHARSET utf8
  COLLATE utf8_general_ci;

USE apidocs;

-- ----------------------------
-- Table structure for ad_group
-- ----------------------------
DROP TABLE IF EXISTS `ad_group`;
CREATE TABLE `ad_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '分组名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='需求分组表';

-- ----------------------------
-- Table structure for ad_interface
-- ----------------------------
DROP TABLE IF EXISTS `ad_interface`;
CREATE TABLE `ad_interface` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '接口名称',
  `type` varchar(64) DEFAULT NULL COMMENT '接口请求的类型，一般有四个枚举值：get、post、put、delete',
  `url` varchar(1024) DEFAULT NULL COMMENT '接口地址',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  `requirement_id` int(11) DEFAULT NULL COMMENT '需求表（ad_requirement）的id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='接口表';

-- ----------------------------
-- Table structure for ad_request
-- ----------------------------
DROP TABLE IF EXISTS `ad_request`;
CREATE TABLE `ad_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(512) DEFAULT NULL COMMENT '参数中文名称',
  `field` varchar(512) DEFAULT NULL COMMENT '参数名',
  `type` varchar(255) DEFAULT NULL COMMENT '参数类型',
  `remark` varchar(1024) DEFAULT NULL COMMENT '参数说明',
  `interface_id` int(11) DEFAULT NULL COMMENT '接口表（ad_interface）的id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='请求参数表';

-- ----------------------------
-- Table structure for ad_requirement
-- ----------------------------
DROP TABLE IF EXISTS `ad_requirement`;
CREATE TABLE `ad_requirement` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) NOT NULL COMMENT '需求名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  `group_id` int(11) DEFAULT NULL COMMENT '需求分组（ad_group）的id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='需求表';

-- ----------------------------
-- Table structure for ad_response
-- ----------------------------
DROP TABLE IF EXISTS `ad_response`;
CREATE TABLE `ad_response` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(512) DEFAULT NULL COMMENT '参数中文名称',
  `field` varchar(512) DEFAULT NULL COMMENT '参数名',
  `type` varchar(255) DEFAULT NULL COMMENT '参数类型',
  `db_field` varchar(512) DEFAULT NULL COMMENT '数据库中该字段对应的参数名',
  `db_type` varchar(255) DEFAULT NULL COMMENT '数据库中该字段对应的参数类型',
  `remark` varchar(1024) DEFAULT NULL COMMENT '参数说明',
  `source` varchar(255) DEFAULT NULL COMMENT '字段来源，即来源于哪一张数据表',
  `interface_id` int(11) DEFAULT NULL COMMENT '接口表（ad_interface）的id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='返回参数表';
